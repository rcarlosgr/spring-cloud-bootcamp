package com.bootcamp.consultamedica.repositories;

import com.bootcamp.consultamedica.entities.DetalleConsulta;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DetalleConsultaRepository extends MongoRepository<DetalleConsulta, String> {
    List<DetalleConsulta> findByIdConsulta(int idConsulta);
}
