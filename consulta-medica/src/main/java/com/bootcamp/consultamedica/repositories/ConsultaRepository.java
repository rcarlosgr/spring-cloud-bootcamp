package com.bootcamp.consultamedica.repositories;

import com.bootcamp.consultamedica.entities.Consulta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConsultaRepository extends JpaRepository<Consulta, Integer> {
    List<Consulta> findByPaciente_IdPaciente(int idPaciente);
    List<Consulta> findByMedico_IdMedico(int idMedico);
    List<Consulta> findByEspecialidad_IdEspecialidad(int idEspecialidad);
}
