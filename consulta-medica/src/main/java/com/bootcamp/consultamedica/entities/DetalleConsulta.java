package com.bootcamp.consultamedica.entities;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "detalleConsulta")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class DetalleConsulta {

    @Id
    private String id;

    @Column(name = "diagnostico", nullable = false, length = 200)
    private String diagnostico;
    @Column(name = "tratamiento", nullable = false, length = 250)
    private String tratamiento;
    @Column(name = "idConsulta", nullable = false)
    private int idConsulta;

}
