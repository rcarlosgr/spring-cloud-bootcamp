package com.bootcamp.consultamedica.entities;

import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "consultaExamen")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ConsultaExamen {
    @Id
    private String id;

    @JoinColumn(name = "idExamen", nullable = false)
    private int idExamen;

    @JoinColumn(name = "idConsulta", nullable = false)
    private int idConsulta;
}
