package com.bootcamp.consultamedica.controllers;

import com.bootcamp.consultamedica.dtos.ConsultaRequestDTO;
import com.bootcamp.consultamedica.dtos.ConsultaWithDetalleResponseDTO;
import com.bootcamp.consultamedica.entities.Consulta;
import com.bootcamp.consultamedica.services.ConsultaService;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/consultas")
@RequiredArgsConstructor
public class ConsultaController {
    private static final Logger logger = LogManager.getLogger(MedicoController.class);

    private final ConsultaService service;

    @GetMapping
    public ResponseEntity<List<Consulta>> getAll() throws Exception {
        return ResponseEntity.ok(service.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Consulta> getById(@PathVariable("id") int id) throws Exception {
        return ResponseEntity.ok(service.getById(id));
    }

    @GetMapping("/paciente/{id}")
    public ResponseEntity<List<Consulta>> getAllByPaciente(@PathVariable("id") int id) {
        return ResponseEntity.ok(service.getAllByPaciente(id));
    }

    @GetMapping("/medico/{id}")
    public ResponseEntity<List<Consulta>> getAllByMedico(@PathVariable("id") int id) {
        return ResponseEntity.ok(service.getAllByMedico(id));
    }

    @GetMapping("/especialidad/{id}")
    public ResponseEntity<List<Consulta>> getAllByEspecialidad(@PathVariable("id") int id) {
        return ResponseEntity.ok(service.getAllByEspecialidad(id));
    }

    @GetMapping("/{id}/detalles")
    public ResponseEntity<ConsultaWithDetalleResponseDTO> getAllWithDetalle(@PathVariable("id") int id) {
        try {
            return ResponseEntity.ok(service.getAllWithDetalle(id));
        } catch (Exception e) {
            logger.error("Error al listar " + e.getMessage());
            e.printStackTrace();
            return ResponseEntity.badRequest().body(new ConsultaWithDetalleResponseDTO());
        }
    }

    @PostMapping
    public ResponseEntity<Consulta> save(@RequestBody ConsultaRequestDTO req) {
        logger.info("Registrar nuevo");
        try {
            Consulta consulta = service.save(req);
            return ResponseEntity.ok(consulta);
        } catch (Exception e) {
            logger.error("No se pudo registrar " + e.getMessage());
            e.printStackTrace();
            return ResponseEntity.badRequest().body(new Consulta());
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Consulta> update(@PathVariable("id") int id, @RequestBody ConsultaRequestDTO req) {
        try {
            Consulta consulta = service.update(id, req);
            return ResponseEntity.ok(consulta);
        } catch (Exception e) {
            logger.error("No se pudo registrar " + e.getMessage());
            e.printStackTrace();
            return ResponseEntity.badRequest().body(new Consulta());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") int id) {
        service.delete(id);
    }
}
