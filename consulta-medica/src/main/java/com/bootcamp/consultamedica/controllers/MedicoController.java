package com.bootcamp.consultamedica.controllers;

import com.bootcamp.consultamedica.entities.Medico;
import com.bootcamp.consultamedica.services.MedicoService;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/medicos")
@RequiredArgsConstructor
public class MedicoController {

    private static final Logger logger = LogManager.getLogger(MedicoController.class);

    private final MedicoService service;

    @GetMapping
    public ResponseEntity<List<Medico>> getAll() throws Exception {
        return ResponseEntity.ok(service.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Medico> getById(@PathVariable("id") int id) throws Exception {
        return ResponseEntity.ok(service.getById(id));
    }

    @PostMapping
    public ResponseEntity<Medico> save(@RequestBody Medico ob) {
        logger.info("Registrar nuevo médico");
        try {
            service.save(ob);
            return ResponseEntity.ok(ob);
        } catch (Exception e) {
            logger.error("No se pudo registrar el médico");
            logger.debug(e);
            return ResponseEntity.badRequest().body(ob);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Medico> update(@PathVariable int id, @RequestBody Medico ob) throws Exception {
        try {
            service.update(id, ob);
            return ResponseEntity.ok(ob);
        } catch (Exception e) {
            logger.error("No se pudo registrar");
            logger.debug(e.getMessage());
            return ResponseEntity.badRequest().body(ob);
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") int id) {
        service.delete(id);
    }
}
