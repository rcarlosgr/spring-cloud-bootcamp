package com.bootcamp.consultamedica.controllers;

import com.bootcamp.consultamedica.entities.DetalleConsulta;
import com.bootcamp.consultamedica.services.DetalleConsultaService;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/detalle-consulta")
@RequiredArgsConstructor
public class DetalleConsultaController {
    private static final Logger logger = LogManager.getLogger(MedicoController.class);

    private final DetalleConsultaService service;

    @GetMapping
    public ResponseEntity<List<DetalleConsulta>> getAll() throws Exception {
        return ResponseEntity.ok(service.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<DetalleConsulta> getById(@PathVariable("id") String id) throws Exception {
        return ResponseEntity.ok(service.getById(id));
    }

    @GetMapping("/consulta/{id}")
    public ResponseEntity<List<DetalleConsulta>> getAllByConsulta(@PathVariable("id") int id) throws Exception {
        return ResponseEntity.ok(service.getAllByConsulta(id));
    }

    @PostMapping
    public ResponseEntity<DetalleConsulta> save(@RequestBody DetalleConsulta ob) {
        logger.info("Registrar nuevo médico");
        try {
            service.save(ob);
            return ResponseEntity.ok(ob);
        } catch (Exception e) {
            logger.error("No se pudo registrar el médico");
            logger.debug(e);
            return ResponseEntity.badRequest().body(ob);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<DetalleConsulta> update(@PathVariable("id") String id, @RequestBody DetalleConsulta ob) throws Exception {
        try {
            service.update(id, ob);
            return ResponseEntity.ok(ob);
        } catch (Exception e) {
            logger.error("No se pudo registrar");
            logger.debug(e);
            return ResponseEntity.badRequest().body(ob);
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") String id) {
        service.delete(id);
    }
}
