package com.bootcamp.consultamedica.controllers;

import com.bootcamp.consultamedica.entities.Examen;
import com.bootcamp.consultamedica.services.ExamenService;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/examenes")
@RequiredArgsConstructor
public class ExamenController {
    private static final Logger logger = LogManager.getLogger(MedicoController.class);
    private final ExamenService service;

    @GetMapping
    public ResponseEntity<List<Examen>> getAll() throws Exception {
        return ResponseEntity.ok(service.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Examen> getById(@PathVariable("id") int id) throws Exception {
        return ResponseEntity.ok(service.getById(id));
    }

    @PostMapping
    public ResponseEntity<Examen> save(@RequestBody Examen ob) {
        logger.info("Registrar nuevo");
        try {
            service.save(ob);
            return ResponseEntity.ok(ob);
        } catch (Exception e) {
            logger.error("No se pudo registrar");
            e.printStackTrace();
            logger.debug(e);
            return ResponseEntity.badRequest().body(ob);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Examen> update(@PathVariable("id") int id, @RequestBody Examen ob) throws Exception {
        try {
            service.update(id, ob);
            return ResponseEntity.ok(ob);
        } catch (Exception e) {
            logger.error("No se pudo registrar");
            logger.debug(e);
            return ResponseEntity.badRequest().body(ob);
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") int id) {
        service.delete(id);
    }
}
