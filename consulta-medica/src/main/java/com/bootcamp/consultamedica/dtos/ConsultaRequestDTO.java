package com.bootcamp.consultamedica.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ConsultaRequestDTO {
    private LocalDate fecha;
    private int idMedico;
    private int idEspecialidad;
    private int idPaciente;
}
