package com.bootcamp.consultamedica.dtos;

import com.bootcamp.consultamedica.entities.DetalleConsulta;
import com.bootcamp.consultamedica.entities.Especialidad;
import com.bootcamp.consultamedica.entities.Medico;
import com.bootcamp.consultamedica.entities.Paciente;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ConsultaWithDetalleResponseDTO {
    private int idConsulta;
    private LocalDate fecha;
    private Medico medico;
    private Especialidad especialidad;
    private Paciente paciente;
    private List<DetalleConsulta> detalleConsultas;
}
