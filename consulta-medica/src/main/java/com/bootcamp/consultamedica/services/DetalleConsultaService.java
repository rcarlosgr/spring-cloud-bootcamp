package com.bootcamp.consultamedica.services;


import com.bootcamp.consultamedica.entities.DetalleConsulta;

import java.util.List;

public interface DetalleConsultaService {
    List<DetalleConsulta> getAll() throws Exception;
    DetalleConsulta getById(String id) throws Exception;
    List<DetalleConsulta> getAllByConsulta(int id);
    DetalleConsulta save(DetalleConsulta ob) throws Exception;
    DetalleConsulta update(String id, DetalleConsulta ob) throws Exception;
    void delete(String id);
}
