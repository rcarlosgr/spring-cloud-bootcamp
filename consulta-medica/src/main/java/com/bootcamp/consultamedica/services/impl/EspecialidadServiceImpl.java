package com.bootcamp.consultamedica.services.impl;

import com.bootcamp.consultamedica.entities.Especialidad;
import com.bootcamp.consultamedica.repositories.EspecialidadRepository;
import com.bootcamp.consultamedica.services.EspecialidadService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class EspecialidadServiceImpl implements EspecialidadService {
    private final EspecialidadRepository repository;

    @Override
    public List<Especialidad> getAll() throws Exception {
        return repository.findAll();
    }

    @Override
    public Especialidad getById(int id) throws Exception {
        Optional<Especialidad> ob = repository.findById(id);
        return ob.orElseGet(Especialidad::new);
    }

    @Override
    public Especialidad save(Especialidad ob) throws Exception {
        return repository.save(ob);
    }

    @Override
    public Especialidad update(int id, Especialidad ob) throws Exception {
        Especialidad especialidad = repository.findById(id).orElseThrow(() -> new Exception("No existe registro"));
        ob.setIdEspecialidad(especialidad.getIdEspecialidad());
        return repository.save(ob);
    }

    @Override
    public void delete(int id) {
        repository.deleteById(id);
    }
}
