package com.bootcamp.consultamedica.services;


import com.bootcamp.consultamedica.dtos.ConsultaRequestDTO;
import com.bootcamp.consultamedica.dtos.ConsultaWithDetalleResponseDTO;
import com.bootcamp.consultamedica.entities.Consulta;

import java.util.List;

public interface ConsultaService {
    List<Consulta> getAll() throws Exception;
    Consulta getById(int id) throws Exception;
    List<Consulta> getAllByPaciente(int id);
    List<Consulta> getAllByMedico(int id);
    List<Consulta> getAllByEspecialidad(int id);
    ConsultaWithDetalleResponseDTO getAllWithDetalle(int id) throws Exception;
    Consulta save(ConsultaRequestDTO ob) throws Exception;
    Consulta update(int id, ConsultaRequestDTO ob) throws Exception;
    void delete(int id);
}
