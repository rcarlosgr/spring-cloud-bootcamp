package com.bootcamp.consultamedica.services;


import com.bootcamp.consultamedica.entities.Especialidad;

import java.util.List;

public interface EspecialidadService {
    List<Especialidad> getAll() throws Exception;
    Especialidad getById(int id) throws Exception;
    Especialidad save(Especialidad ob) throws Exception;
    Especialidad update(int id, Especialidad ob) throws Exception;
    void delete(int id);
}
