package com.bootcamp.consultamedica.services.impl;

import com.bootcamp.consultamedica.dtos.ConsultaRequestDTO;
import com.bootcamp.consultamedica.dtos.ConsultaWithDetalleResponseDTO;
import com.bootcamp.consultamedica.entities.*;
import com.bootcamp.consultamedica.repositories.*;
import com.bootcamp.consultamedica.services.ConsultaService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ConsultaServiceImpl implements ConsultaService {
    private final ConsultaRepository repository;
    private final MedicoRepository medicoRepository;
    private final PacienteRepository pacienteRepository;
    private final EspecialidadRepository especialidadRepository;
    private final DetalleConsultaRepository detalleConsultaRepository;

    @Override
    public List<Consulta> getAll() throws Exception {
        return repository.findAll();
    }

    @Override
    public Consulta getById(int id) throws Exception {
        Optional<Consulta> ob = repository.findById(id);
        return ob.orElseGet(Consulta::new);
    }

    @Override
    public List<Consulta> getAllByPaciente(int id) {
        return repository.findByPaciente_IdPaciente(id);
    }

    @Override
    public List<Consulta> getAllByMedico(int id) {
        return repository.findByMedico_IdMedico(id);
    }

    @Override
    public List<Consulta> getAllByEspecialidad(int id) {
        return repository.findByEspecialidad_IdEspecialidad(id);
    }

    @Override
    public ConsultaWithDetalleResponseDTO getAllWithDetalle(int id) throws Exception {
        Consulta consulta = repository.findById(id).orElseThrow(() -> new Exception("No existe registro"));
         ConsultaWithDetalleResponseDTO responseDTO = new ConsultaWithDetalleResponseDTO();
        responseDTO.setIdConsulta(consulta.getIdConsulta());
        responseDTO.setFecha(consulta.getFecha());
        responseDTO.setMedico(consulta.getMedico());
        responseDTO.setPaciente(consulta.getPaciente());
        responseDTO.setEspecialidad(consulta.getEspecialidad());

        List<DetalleConsulta> detalleConsultas = detalleConsultaRepository.findByIdConsulta(consulta.getIdConsulta());
        responseDTO.setDetalleConsultas(detalleConsultas);
        return responseDTO;
    }

    @Override
    public Consulta save(ConsultaRequestDTO req) throws Exception {
        Medico medico = medicoRepository.findById(req.getIdMedico()).orElseThrow(() -> new Exception("No existe id de medico"));
        Paciente paciente = pacienteRepository.findById(req.getIdPaciente()).orElseThrow(() -> new Exception("No existe id de paciente"));
        Especialidad especialidad = especialidadRepository.findById(req.getIdPaciente()).orElseThrow(() -> new Exception("No existe id de especialidad"));

        Consulta consulta = new Consulta();
        consulta.setFecha(req.getFecha());
        consulta.setMedico(medico);
        consulta.setPaciente(paciente);
        consulta.setEspecialidad(especialidad);

        return repository.save(consulta);
    }

    @Override
    public Consulta update(int id, ConsultaRequestDTO req) throws Exception {
        Consulta consulta = repository.findById(id).orElseThrow(() -> new Exception("No existe registro"));
        Medico medico = medicoRepository.findById(req.getIdMedico()).orElseThrow(() -> new Exception("No existe id de medico"));
        Paciente paciente = pacienteRepository.findById(req.getIdPaciente()).orElseThrow(() -> new Exception("No existe id de paciente"));
        Especialidad especialidad = especialidadRepository.findById(req.getIdPaciente()).orElseThrow(() -> new Exception("No existe id de especialidad"));

        consulta.setFecha(req.getFecha());
        consulta.setMedico(medico);
        consulta.setPaciente(paciente);
        consulta.setEspecialidad(especialidad);

        return repository.save(consulta);
    }

    @Override
    public void delete(int id) {
        repository.deleteById(id);
    }
}
