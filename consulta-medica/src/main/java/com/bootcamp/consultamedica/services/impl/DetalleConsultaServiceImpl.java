package com.bootcamp.consultamedica.services.impl;

import com.bootcamp.consultamedica.controllers.MedicoController;
import com.bootcamp.consultamedica.entities.DetalleConsulta;
import com.bootcamp.consultamedica.repositories.DetalleConsultaRepository;
import com.bootcamp.consultamedica.services.DetalleConsultaService;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DetalleConsultaServiceImpl implements DetalleConsultaService {
    private static final Logger logger = LogManager.getLogger(MedicoController.class);
    private final DetalleConsultaRepository repository;

    @Override
    public List<DetalleConsulta> getAll() throws Exception {
        return repository.findAll();
    }

    @Override
    public DetalleConsulta getById(String id) throws Exception {
        Optional<DetalleConsulta> ob = repository.findById(id);
        return ob.orElseGet(DetalleConsulta::new);
    }

    @Override
    public List<DetalleConsulta> getAllByConsulta(int id) {
        return repository.findByIdConsulta(id);
    }

    @Override
    public DetalleConsulta save(DetalleConsulta ob) throws Exception {
        logger.info("guardando DetalleConsulta");
        return repository.save(ob);
    }

    @Override
    public DetalleConsulta update(String id, DetalleConsulta ob) throws Exception {
        DetalleConsulta detalleConsulta = repository.findById(id).orElseThrow(() -> new Exception("No existe registro"));
        ob.setId(detalleConsulta.getId());
        return repository.save(ob);
    }

    @Override
    public void delete(String id) {
        repository.deleteById(id);
    }
}
