package com.bootcamp.consultamedica.services.impl;

import com.bootcamp.consultamedica.entities.Paciente;
import com.bootcamp.consultamedica.repositories.PacienteRepository;
import com.bootcamp.consultamedica.services.PacienteService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PacienteServiceImpl implements PacienteService {
    private final PacienteRepository repository;
    @Override
    public List<Paciente> getAll() throws Exception {
        return repository.findAll();
    }

    @Override
    public Paciente getById(int id) throws Exception {
        Optional<Paciente> ob = repository.findById(id);
        return ob.orElseGet(Paciente::new);
    }

    @Override
    public Paciente save(Paciente ob) throws Exception {
        return repository.save(ob);
    }

    @Override
    public Paciente update(int id, Paciente ob) throws Exception {
        Paciente paciente = repository.findById(id).orElseThrow(() -> new Exception("No existe registro"));
        ob.setIdPaciente(paciente.getIdPaciente());
        return repository.save(ob);
    }

    @Override
    public void delete(int id) {
        repository.deleteById(id);
    }
}
