package com.bootcamp.consultamedica.services.impl;

import com.bootcamp.consultamedica.entities.Medico;
import com.bootcamp.consultamedica.repositories.MedicoRepository;
import com.bootcamp.consultamedica.services.MedicoService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MedicoServiceImpl implements MedicoService {
    private final MedicoRepository repository;

    @Override
    public List<Medico> getAll() throws Exception {
        return repository.findAll();
    }

    @Override
    public Medico getById(int id) throws Exception {
        Optional<Medico> ob = repository.findById(id);
        return ob.orElseGet(Medico::new);
    }

    @Override
    public Medico save(Medico ob) throws Exception {
        return repository.save(ob);
    }

    @Override
    public Medico update(int id, Medico ob) throws Exception {
        Medico medico = repository.findById(id).orElseThrow(() -> new Exception("No existe registro"));
        ob.setIdMedico(medico.getIdMedico());
        return repository.save(ob);
    }

    @Override
    public void delete(int id) {
        repository.deleteById(id);
    }
}
