package com.bootcamp.consultamedica.services.impl;

import com.bootcamp.consultamedica.entities.Examen;
import com.bootcamp.consultamedica.repositories.ExamenRepository;
import com.bootcamp.consultamedica.services.ExamenService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ExamenServiceImpl implements ExamenService {
    private final ExamenRepository repository;

    @Override
    public List<Examen> getAll() throws Exception {
        return repository.findAll();
    }

    @Override
    public Examen getById(int id) throws Exception {
        Optional<Examen> ob = repository.findById(id);
        return ob.orElseGet(Examen::new);
    }

    @Override
    public Examen save(Examen ob) throws Exception {
        return repository.save(ob);
    }

    @Override
    public Examen update(int id, Examen ob) throws Exception {
        Examen examen = repository.findById(id).orElseThrow(() -> new Exception("No existe registro"));
        ob.setIdExamen(examen.getIdExamen());
        return repository.save(ob);
    }

    @Override
    public void delete(int id) {
        repository.deleteById(id);
    }
}
