package com.bootcamp.consultamedica.services;


import com.bootcamp.consultamedica.entities.Paciente;

import java.util.List;

public interface PacienteService {
    List<Paciente> getAll() throws Exception;
    Paciente getById(int id) throws Exception;
    Paciente save(Paciente ob) throws Exception;
    Paciente update(int id, Paciente ob) throws Exception;
    void delete(int id);
}
