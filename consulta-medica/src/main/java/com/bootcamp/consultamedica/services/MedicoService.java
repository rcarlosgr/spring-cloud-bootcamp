package com.bootcamp.consultamedica.services;

import com.bootcamp.consultamedica.entities.Medico;

import java.util.List;

public interface MedicoService {
    List<Medico> getAll() throws Exception;
    Medico getById(int id) throws Exception;
    Medico save(Medico ob) throws Exception;
    Medico update(int id,  Medico ob) throws Exception;
    void delete(int id);
}
