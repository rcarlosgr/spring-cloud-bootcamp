package com.bootcamp.consultamedica.services;


import com.bootcamp.consultamedica.entities.Examen;

import java.util.List;

public interface ExamenService {
    List<Examen> getAll() throws Exception;
    Examen getById(int id) throws Exception;
    Examen save(Examen ob) throws Exception;
    Examen update(int id, Examen ob) throws Exception;
    void delete(int id);
}
